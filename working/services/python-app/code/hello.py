# http://flask.pocoo.org/docs/0.12/quickstart/

from flask import Flask, url_for
import redis

redis_client = redis.StrictRedis(host='redis-db', port=6381)
app = Flask(__name__)


@app.route('/')
def home():
    return """
        <p>Welcome, (sorry Jeppe).--<a href=\"%s\"> Visitor Count</a></p>
    """ % url_for('visitor_count')

@app.route('/visitor_count')
def visitor_count():
    visitor_counter = redis_client.incr('visitor_counter')
    return """
        <p>You are visitor numero: %s.--<a href=\"%s\"> Home</a></p>
    """ % (visitor_counter, url_for('home'))

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
